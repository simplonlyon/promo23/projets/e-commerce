# Projet e-commerce

L'objectif de ce projet sera de réaliser un site de vente en ligne par groupes de 3-4 personnes en 4-5 semaines.
A votre équipe de trouver un sujet innovant et motivant (Que vous seriez fier·e de présenter à des recrutements).
Le thème du site est "libre" (ça veut dire que si le thème est éthiquement limite, il sera refusé)
Les contraintes sont les suivantes : 
## Conception
* Réalisation de diagrammes de Use Case
* Réalisation d'un diagramme de classe
* Réalisation de maquettes (wireframe) mobile first (rendre les maquettes navigables)
## Technique
* Utilisation Symfony et Doctrine pour le back
* Utilisation de React/Next pour le front
* Application responsive (Via bootstrap, ou des library comme Material-UI, ou Ant Design)
* Utilisation de git/gitlab pour le travail en groupe
## Organisation Agile
* 4 sprints (un par semaine de code) avec les fonctionnalités à livrer
    * Au début de chaque sprint, définir le livrable attendu, les tâches à réaliser
    * En début de chaque après midi, faire un daily scrum de 15 minutes max pour échanger sur tâches en cours et vos éventuels soucis
    * Terminer la semaine par une retrospective d'équipe avec un formateur
* Utiliser la partie Issues et les Boards du projet gitlab pour définir vos tâches et votre kanban

Vous réaliserez également un README avec vos différents diagrammes commentés et une petite présentation du projet.


## User Stories obligatoires

1. En tant qu'utilisateur·ice, je veux pouvoir visualiser la liste des produits filtrés pour faciliter mon choix
2. En tant qu'utilisateur·ice, je veux pouvoir ajouter des produits à mon panier pour passer rapidement ma commande
3. En tant que visiteur·euse, je veux pouvoir créer mon compte pour accéder à des fonctionnalités supplémentaires du site
4. En tant qu'administrateur·ice, je veux pouvoir gérer les produits disponibles afin de modifier le catalogue
